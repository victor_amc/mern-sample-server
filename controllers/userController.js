const User = require('../models/User');
const bcryptjs = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');

exports.createUser = async(req, res) => 
{
    //evaluate for errors
    const errors = validationResult(req);
    if (!errors.isEmpty())
    {
        return res.status(400).json({errors: errors.array()});
    }

    //get email and password from the request.
    const {email, password} =  req.body;

    try
    {
        //validate
        let user = await User.findOne({email});
        if (user) return res.status(400).send({ msg: 'User already exist.'});

        //create new User
        user = new User(req.body);

        //hash password
        const salt = await bcryptjs.genSalt(10);
        user.password= await bcryptjs.hash(password, salt);

        //save user
        await user.save();

        //create JWT
        const payload = {user: {id: user.id}};

        //sign JWT & Send Response
        jwt.sign(payload, process.env.SECRET, {expiresIn: 3600}, 
            (error, token) => {
                if(error) throw error;
                
                //Confirmation message will be the token
                res.json({ token});
            
        }); //payload, secret, 1 hour, callback
    }
    catch (error)
    {
        console.log(error);
        res.status(400).send({msg: `An error was caught: ${error}`});
    }
    
};



