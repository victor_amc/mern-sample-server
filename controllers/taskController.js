const Task = require('../models/Task');
const Project = require('../models/Project');
const auth = require('../middleware/auth');
const {validationResult} = require('express-validator');


//1. Create Task 
exports.createTask = async(req, res) => {

    //Check for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }

    try{
        //Extract project 
        const taskSent = req.body;
        const project = await Project.findById(taskSent.projectId);
        
        //Evaluate if exists
        if (!project){
            return res.status(404).json({msg: 'Project not found.'});
        }

        //Evaluate if current project belongs to authenticated user.
        if (project.author.toString() !== req.user.id){
            return res.status(401).json({msg: 'Action not Authorised'});
        }

        //Create task
        const task = new Task(req.body);
        await task.save();
        res.json(task);

    }
    catch(error){
        console.log(error);
        res.status(500).send({msg:'Server error detected'});
    }

};  

//2. GET project Tasks
exports.getTasks = async (req, res) => {

    try {

        //Extract project 
        const {projectId} = req.query; //query is used when passed params.
        const dbProject = await Project.findById(projectId);

        //Evaluate if exists
        if (!dbProject){
            return res.status(404).json({msg: 'Project not found.'});
        }

        //Evaluate if current project belongs to authenticated user.
        if (dbProject.author.toString() !== req.user.id){
            return res.status(401).json({msg: 'Action not Authorised'});
        }

        //Get tasks
        const tasks = await Task.find({ projectId }).sort({ created: -1 }); //where project
        res.json({tasks});

    }   
    catch(error){
        console.log(error);
        res.status(500).send({msg:'Server error detected'});
    }

}

//3. UPDATE Task
exports.updateTask = async (req, res) => {

    //Check for errors
    const errors = validationResult(req);
    if (!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }

    try {
        //Get params
        const {projectId, name, status} = req.body;
        const taskId = req.params.id; //from /:id

        //Extract project & task
        const dbProject = await Project.findById(projectId);
        let dbTask = await Task.findById(taskId); 

        //Evaluate if task exists
        if (!dbTask){
            return res.status(404).json({msg:'Task does not exist'});
        }
        
        //Evaluate if current project belongs to authenticated user.
        if (dbProject.author.toString() !== req.user.id){
            return res.status(401).json({msg: 'Action not Authorised'});
        }

        //create new Task
        const newTask = {};
        newTask.name = name; 
        newTask.status = status; 

        dbTask = await Task.findOneAndUpdate({_id: req.params.id}, 
                                            newTask, 
                                            {new :true});

        res.json(dbTask);

    }   
    catch(error){
        console.log(error);
        res.status(500).send({msg:'Server error detected'});
    }
}

//4. DELTE Task
exports.deleteTask = async (req, res) => {

    try {
        //Extract project & task
        const {projectId} = req.query;
        const dbProject = await Project.findById(projectId);

        //Evaluate if current project belongs to authenticated user.
        if (dbProject.author.toString() !== req.user.id){
            return res.status(401).json({msg: 'Action not Authorised'});
        }

        //Delete Project
        //https://www.geeksforgeeks.org/mongoose-findoneandremove-function/?ref=lbp
        await Task.findOneAndRemove({_id: req.params.id}, 
            function (err, del_task) { 
                if (err){ 
                    res.json({msg: `${err}`});
                } 
                else{ 
                    res.json({msg: `Deleted task: ${del_task.name}`});
                }
            });
        
    }   
    catch(error){
        console.log(error);
        res.status(500).send({msg:'Server error detected'});
    }
}