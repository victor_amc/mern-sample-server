const User = require('../models/User');
const bcryptjs = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');


//AUTHENTICATE user
exports.authenticateUser = async(req, res) => 
{
    //evaluate for errors
    const errors = validationResult(req);
    if (!errors.isEmpty())
    {
        return res.status(400).json({errors: errors.array()});
    }

    //get email and password from the request.
    const {email, password} =  req.body;

    try
    {
        //evaluate is a user
        let user = await User.findOne({email});
        if (!user) {res.status(400).json({msg:'User does not exist.'});}

        //evaluate password
        const isValidPassword = await bcryptjs.compare(password, user.password);
        if (!isValidPassword){res.status(400).json({msg:'Invalid password.'});}
        
        //Create JWT
        const payload = {user: {id: user.id}};

        //Sign JWT & Send Response (expireIn: Seconds (3600 = 1 hr))
        jwt.sign(payload, process.env.SECRET, {expiresIn: 10800}, 
            (error, token) => {
                if(error) throw error;
                
                //Confirmation message will be the token
                res.json({ token});
            
        }); //payload, secret, 1 hour, callback
    }
    catch (error)
    {
        console.log(error);
        res.status(400).send({msg:`Server error detected: ${error}`});
    }
    
};

// GET user 
exports.getUser = async (req, res) => {
    try{
        //(req.user.id) was stored in middlewear/auth 
        const user = await User.findById(req.user.id).select('-password'); //mognoose .select() minus = all except
        res.json({user});
    }
    catch (error)
    {
        console.log(error);
        res.status(400).send({msg:`Server error detected: ${error}`});
    }
}



