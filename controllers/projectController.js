const Project = require('../models/Project');
const {validationResult} = require('express-validator');

//1. CREATE new project
exports.createProject = async (req, res) => {

    //evaluate for errors
    const errors = validationResult(req);
    if (!errors.isEmpty())
    {
        return res.status(400).json({errors: errors.array()});
    }


    try{

        //Create a new project
        const project = new Project(req.body);

        //Save Author with JWT
        project.author = req.user.id;

        //store project in db
        project.save();
        res.json(project);

    }
    catch(error){
        console.log('error');
        res.status(500).send({msg:'Server error detected'});
    }
}


//2. GET current user Projects

exports.getProjects = async (req, res) => {
    try{
        //queries for the projects with the "author" with the same id as the current token stored.
        const projects = await Project.find({ author: req.user.id })
            .sort({date: -1});
        res.json({projects});
    }
    catch (error) {
        console.log('error');
        res.status(500).send({msg:'Server error detected'});
    }
}

//3. UPDATE project 
exports.updateProject = async (req, res) => {

    //evaluate for errors
    const errors = validationResult(req);
    if (!errors.isEmpty())
    {
        return res.status(400).json({errors: errors.array()});
    }

    //extract project data
    const {name} = req.body;
    const newProject = {};
    
    //get new 
    if (name){newProject.name = name;}

    try{
        //evaluate  ID
        let project = await Project.findById(req.params.id);

        //evaluate project exists
        if (!project)
        {
            return res.status(404).json({msg: 'Project not found'});
        }

        //verify project author
        if (project.author.toString() !== req.user.id)
        {
            return res.status(401).json({msg: 'Action not Authorised'});
        }

        //update
        project = await Project.findByIdAndUpdate(  {_id: req.params.id}, 
                                                    {$set : newProject},
                                                    {new: true});
        
        res.json({project});
    }
    catch(error){
        console.log(error);
        res.status(500).send({msg:'Server error detected.'});
    }
}

//4. DELETE a Project
exports.deleteProject = async (req, res) => {

    try
    {
        //evaluate  ID
        let project = await Project.findById(req.params.id);

        //evaluate project exists
        if (!project)
        {
            return res.status(404).json({msg: 'Project not found'});
        }

        //verify project author
        if (project.author.toString() !== req.user.id)
        {
            return res.status(401).json({msg: 'Action not Authorised'});
        }

        //delete project
        await Project.findOneAndRemove({_id: req.params.id});
        res.json({msg: 'Deleted project'});

    }
    catch(error){
        console.log(error);
        res.status(500).send({msg:'Server error detected.'});
    }

}