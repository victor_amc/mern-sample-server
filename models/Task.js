const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    status:{
        type: Boolean,
        default: false
    },
    created:{
        type: Date,
        default: Date.now()
    },
    projectId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Project' // As in model
    }

});

module.exports = mongoose.model('Task', TaskSchema);