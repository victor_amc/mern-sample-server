const express = require('express');
const router = express.Router();
const projectController = require('../controllers/projectController');
const auth = require('../middleware/auth');
const {check} = require('express-validator');

//ROUTE: /api/projects

//1. CREATE PROJECT
router.post('/', 
        auth,
        [
            check('name', 'Project Name is required.').not().isEmpty()
        ],
        projectController.createProject
    );

//2. GET USER PROJECTS 
router.get('/', 
    auth,
    projectController.getProjects
);

//3. UPDATE USER PROJECT 
// :id is a proxy for the project id
router.put('/:id', 
    auth,
    [
        check('name', 'Project Name is required.').not().isEmpty()
    ],
    projectController.updateProject
);

//4. DELETE USER PROJECT 
// :id is a proxy for the project id
router.delete('/:id', 
    auth,
    projectController.deleteProject
);


module.exports = router;

//Node express can work with multiple middlewares by separating them with a comma.
//In this case we authenticat the JWT token
//Then we check with express that the project's name is inserted
//Then we create the project.