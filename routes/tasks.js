const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');
const auth = require('../middleware/auth');
const {check} = require('express-validator');


//ROUTE: /api/tasks

//1. CREATE Task
router.post('/', 
        auth,
        [
            check('name','Name is requried.').not().isEmpty(),
            check('projectId','Project is requried.').not().isEmpty(),
        ],
        taskController.createTask
        );

//2. GET project Tasks
router.get('/', 
        auth,
        [
            check('projectId','Project is requried.').not().isEmpty(),
        ],
        taskController.getTasks
        );

//3. UPDATE Task
// :id is a key proxy for the task id
router.put('/:id', 
        auth,
        [
            check('projectId','Project is requried.').not().isEmpty(),
        ],
        taskController.updateTask
        );

//4. DELETE Task
// :id is a key proxy for the task id
router.delete('/:id', 
        auth,
        [
            check('projectId','Project is requried.').not().isEmpty(),
        ],
        taskController.deleteTask
        );



module.exports = router;


