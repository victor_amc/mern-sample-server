//routes to authentica users
const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const auth = require('../middleware/auth')
const authController = require('../controllers/authController')

//ROUTE: /api/auth

//1. CREATE USER
router.post('/', 
    [
        check('email', 'Add a valid email').isEmail(),
        check('password', 'Password is minimm 6 charactes').isLength({min: 6})
    ],
    authController.authenticateUser
);

//2. GET USER AUTH
router.get('/', 
        auth, 
        authController.getUser
        )

module.exports = router;
