//routes to create users
const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const {check} = require('express-validator');

// ROUTE : /api/users

//1. CREATE USER
router.post('/', 
    [
        check('name', 'Name is required').not().isEmpty(),
        check('surname', 'Surname is required').not().isEmpty(),
        check('email', 'Add a valid email').isEmail(),
        check('password', 'Password is minimm 6 charactes').isLength({min: 6})
    ],
    userController.createUser
);

module.exports = router;

//Node express can work with multiple middlewares by separating them with a comma.
//In this casewe send an array of checks & then we create the user.
