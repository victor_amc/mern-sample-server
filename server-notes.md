#SERVER NOTES#

Step by Step process of how to create a user server.

1. Start Npm:
    vscode: Right click on 'server' folder >> Open in integrated Terminal
    
    Terminal:    `npm init`

2. Install Nodemon a virtual environment for development (restarts at any change):

    `npm install -D nodemon`

3. Install Server & ORM (for queries)

    `npm install express mongoose dotenv --save`

    *express (server)
    *mongoose (orm)
    *dotenv (file for environment variables)

4. Set up your scripts.

    Insider package.json insider the script section:

    ```
    "start": "node .", 
    "dev": "nodemon ."
    ```

    First one starts the server with node (would look for main file: index.js).
    Second one will start the development environment.

5. Start Server

    5.1. Create an index.js and test server (npm run dev):

    ```
    const express = require('express');
    
    //create server
    const app = express();

    //app PORT
    //if process exist use PORT else 4000 (react uses 3000)
    const PORT = process.env.PORT || 4000;

    //main router
    //use router in different file
    app.get('/', (req, res) => {
        res.send('Hello World');
    });

    //start app
    app.listen(PORT, () => {
        console.log(`Server WORKING on port: ${PORT}`)
    }); 
    ```

 
6. Set up DB (MongoDB, PostGRES, MySQL)

    6.1. Create an account for any provider, this sample will be with MongoDBAtlas:
        6.1.1 Create Cluster
        6.1.2 Get Connection URL for application
            MongoDB >> Create Project >> Cluster >> Connect >> To Application >> Copy String

    6.2. Create variables.env and put the connection string:

        6.2.1. Create Database and a Table
        
            *Download MongoDB Compass connect & to database by string follow:
            https://www.udemy.com/course/react-de-principiante-a-experto-creando-mas-de-10-aplicaciones/learn/lecture/17671904#notes

            *Add Connection and add it to favorites
            mongodb+srv://admin:<password>@cluster0.uvowh.mongodb.net/test

        6.2.2 Add link to "variables.env" file in this case root and password:
            mongodb+srv://root:<password>@cluster0.v4sn0.mongodb.net/<dbname>?retryWrites=true&w=majority

    6.3 Set up configuration of database.
    
        6.3.1 First create a new file named "db.js" for configuration of database inside a folder "config".

        6.3.2 Set up the db connection as:

        const mongoose = require('mongoose');

        ```
        require('dotenv').config({path:'variables.env'});

        const connectDB = async () => {
            try{
                await mongoose.connect(process.env.DB_MONGO, {
                    useNewUrlParser: true,
                    useFindAndModify: false,
                    useUnifiedTopology: true,
                    useCreateIndex: true
                }); //dotenv with name of db inside variables.env (this case DB_MONGO)
            }
            catch (error)
            {
                console.log(error);
                process.exit(1);
            }
        }

        module.exports = connectDB;
        ```

    6.4. Connect to Database by adding the conneciton to 'index.js':

        ```
        const express = require('express');
        const connectDB = require('./config/db');

        //create server
        const app = express();

        //Connect to DB
        connectDB();

        //Enable express.json (json parser)
        app.use(express.json({extended:true}));

        //app PORT
        //if process exist use PORT else 4000 (react uses 3000)
        const PORT = process.env.PORT || 4000;

        //start app
        app.listen(PORT, () => {
            console.log(`Server WORKING on port: ${PORT}`)
        });
        ```

7. Set up Routes (midleware). Can help with Postman

    7.1 Create a routes folder for all url routes that will be produce.
        e.g. users.js

    ```
    //routes to create users
    const express = require('express');
    const router = express.Router();

    //1. CREATE USER
    //api/users
    router.post('/',()=>{
        console.log('Create User');
    });
    module.exports = router;
    ```

    7.2 Incorporate tue route inside index.js (after connection):
    
    ```
    //import routes
    app.use('/api/users', require('./routes/users')); //middle-ware in express
    ```

    7.3 Create a controller to avoid long db call-strings. Recommended to create a folder with all controllers and a file of controller per routes. Controllers job is to manage reponses and posts of the model.

    In a file "userController.js" go:
    
    ```
    exports.createUser =(req, res) => 
    {
    console.log('from create user')
    };
    ```

    And then substitute in route/users.js the post action:

    ```
    const userController = require('../controllers/userController');

    //1. CREATE USER
    //api/users
    router.post('/', userController.createUser);
    ```

8. Create Models:

    8.1 A model defines the data schema/structure that a post or get action will have. 
    8.2 We should create one for each object. In the example below we craeted 'models/Users.js' (Caps is standard).
        and will contain the schema of my json data:

    ```
    const mongoose = require('mongoose');

    const UserSchema = mongoose.Schema({
        name:{type:String, required: true, trim: true},
        surname:{type:String, required: true, trim: false},
        email:{type:String, required: true, trim: true, unique: true},
        password:{type:String, required: true, trim: true},
        register:{type:Date, default:Date.now()}
    });

    module.exports = mongoose.model('User', UserSchema);
    ```

9. Add object to DB.

    Inside controller (this case being userController) create the new object:

    ```
    const User = require('../models/User')
    const bcryptjs = require('bcryptjs');
    exports.createUser = async(req, res) => 
    {
        try
        {
            //validate
            let user = await User.findOne({email});
            if (user) return res.status(400).send('User already exist.');

            //create new User
            user = new User(req.body);

            //hash password
            const salt = await bcryptjs.genSalt(10);
            user.password= await bcryptjs.hash(password, salt);

            //save user
            await user.save();

            //message
            res.send('User inserted correctly.');
        }
        catch (error)
        {
            console.log(error);
            res.status(400).send(`An error was caught: ${error}`);
        }
    };
    ```

10. Validate Data

    10.1 Install Server validator

    `npm install express-validator --save`

    10.2 Change the route to evaluate the data when posting like:

    ```
    //routes to create users
    const express = require('express');
    const router = express.Router();
    const userController = require('../controllers/userController');
    const {check} = require('express-validator');

    //1. CREATE USER
    //api/users
    router.post('/', 
        [
            check('name', 'Name is required').not().isEmpty(),
            check('email', 'Add a valid email').isEmail(),
            check('password', 'Password is minimm 6 charactes').isLength({min: 6})
        ],
        userController.createUser
    );

    module.exports = router;
    ```

    10.3 Then in the Controller, catch if any error happen from the evaluation in the beginning from:

    ```
    const {check} = require('express-validator');
    exports.createUser = async(req, res) => 
    {

    //evaluate for errors
    const errors = validationResult(req);
    if (!errors.isEmpty())
    {
        return res.satus(400).json({errors: errors.array()});
    }

    //... all the other code...
    }
    ```

11. JWT (Jason Web Token)

    11.1 Definition:    Standard for sharing information between different apps in a JSON object.
                        It allows to shared data securely as it looks for JSON authenticity.
                    
        When to Use:    Common to be used when a user gets logged and its data gets stored in JWT so that 
                        the user gets access to all aplication services and resources while Token is valid.

        Parts:          Header:     JWT type and algorithm.
                        Payload:    Entity information & additional data.
                        Signature:  Verifies that message has not been changed.

    11.2 Install:       npm install jsonwebtoken --save

    11.3 Implementation
    Inside the object controller (in this case userController.js) you have to add the authentication token and sign it.

    ```
    const User = require('../models/User');
    const bcryptjs = require('bcryptjs');
    const {validationResult} = require('express-validator');
    const jwt = require('jsonwebtoken');

    exports.createUser = async(req, res) => 
    {
        //evaluate for errors
        const errors = validationResult(req);
        if (!errors.isEmpty())
        {
            return res.status(400).json({errors: errors.array()});
        }

        //get email and password from the request.
        const {email, password} =  req.body;

        try
        {
            //validate
            let user = await User.findOne({email});
            if (user) return res.status(400).send('User already exist.');

            //create new User
            user = new User(req.body);

            //hash password
            const salt = await bcryptjs.genSalt(10);
            user.password= await bcryptjs.hash(password, salt);

            //save user
            await user.save();

            //Create JWT
            const payload = {user: {id: user.id}};

            //sign JWT & Send Response
            jwt.sign(payload, process.env.SECRET, {expiresIn: 3600}, 
                (error, token) => {
                    if(error) throw error;
                    
                    //Confirmation message will be the token
                    res.json({ token});
                
            }); //payload, secret, 1 hour, callback
        }
        catch (error)
        {
            console.log(error);
            res.status(400).send(`An error was caught: ${error}`);
        }
    };
    ```

    



##EXTRA NOTES##

1. CORS  
    To avoid an issue in the frontend while running the local server in a different route (localhost:3000 vs localhost:4000)

    1.1 Install:   npm i cors
    1.2 Enable cors:
        Inside  index.js

        ```
        const cors = require('cors');

        //after connection to db
        app.use(cors());
        ```


    
