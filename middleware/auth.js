const jwt = require('jsonwebtoken');

module.exports = function(req, res, next){
    //Read token from header
    const token = req.header('x-auth-token');
    console.log(token);

    //Check token is ok
    if (!token){
        return res.status(401).json({msg:'No Token, unvalid permit.'})
    }

    //Validate token
    try{
        const cypher = jwt.verify(token, process.env.SECRET);

        //set in request the user that was stored (in userController payload = user {id})
        req.user = cypher.user;
        
        //Pushes to next middlware
        next();
    }
    catch(error)
    {
        res.status(401).json({msg:'Token not valid.'});
    }

}