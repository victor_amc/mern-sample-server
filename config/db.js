const mongoose = require('mongoose');

require('dotenv').config({path:'variables.env'});

const connectDB = async () => {
    try{
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
            useCreateIndex: true
        }); //dotenv with name of db inside variables.env (this case DB_MONGO)

        console.log('DB Connected!')
    }
    catch (error)
    {
        console.log(error);
        process.exit(1);
    }
}

module.exports = connectDB;