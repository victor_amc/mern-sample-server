
const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors');


//create server
const app = express();

//Connect to DB
connectDB();

//enable CORS
app.use(cors());

//Enable express.json
app.use(express.json({extended:true}));

//app PORT
//if process exist use PORT else 4000 (react uses 3000)
const port = process.env.PORT || 4000;

//import routes (middle-ware in express)
app.use('/api/users', require('./routes/users')); 
app.use('/api/auth', require('./routes/auth')); 
app.use('/api/projects', require('./routes/projects')); 
app.use('/api/tasks', require('./routes/tasks')); 

//start app
app.listen(port, '0.0.0.0' ,() => {
    console.log(`Server WORKING on port: ${port}`)
});

